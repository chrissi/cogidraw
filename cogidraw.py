#!/usr/bin/env python

# copied from:
# https://gitli.stratum0.org/chrissi/cogidraw

#License:
# http://creativecommons.org/licenses/by-sa/4.0/


import wx
import serial
import sys
import argparse

parser = argparse.ArgumentParser(description="cogidraw - Live drawing on a HPGL-Compatible Plotter")
parser.add_argument("--ttyUSB", default="/dev/ttyUSB0", help="Serial the plotter is on")
parser.add_argument("--scaling", default=10, help="Scaling factor from px on screen to tics on plotter", type=int)
args = parser.parse_args()


scale = int(args.scaling)

def outp(line,fi=True):
    if fi:
        sys.stdout.write(line)
        sys.stdout.flush()
    ser.write(line)

class cFrame(wx.Frame):
    isdown = False
    count = 0

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(500,500))
        panel = wx.Panel(self, -1)
        panel.Bind(wx.EVT_MOUSE_EVENTS, self.mouse)
        self.Show(True)

    def mouse(self, evt):
        if evt.ButtonDown():
            self.isdown = True
            outp("IN;IP;SP1;", False)
            pos = evt.GetPosition()
            s = "PU{},{};".format(pos[0]*scale, pos[1]*scale)
            outp(s)

        if evt.ButtonUp():
            self.isdown = False
            outp(" U F U @", False)

        if self.isdown:
            pos = evt.GetPosition()
            s = "PD{},{};".format(pos[0]*scale, pos[1]*scale)
            outp(s) 
            self.count += 1
            
            if self.count == 2:
                ser.write(" U F U @")
                ser.write("IN;IP;SP1;")
                self.count = 0
                
ser = serial.Serial(port=args.ttyUSB)

if not ser.isOpen():
    print "Serial failed."
    exit()

sys.stdout.write("IN;IP;SP1;")

app = wx.App(False)
frame = cFrame(None, 'draw')
app.MainLoop()


